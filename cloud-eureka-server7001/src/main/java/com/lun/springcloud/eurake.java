package com.lun.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @Classname eurake
 * @Description TODO
 * @Date 2021/9/28 3:14 下午
 * @Created by houpeng
 */
@SpringBootApplication
@EnableEurekaServer
public class eurake {
    public static void main(String[] args) {
        SpringApplication.run(eurake.class);
    }
}
