package com.lun.springcloud.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Classname PayMentDao
 * @Description TODO
 * @Date 2021/9/28 11:29 上午
 * @Created by houpeng
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("payment")
public class PayMent {
    public long id;
    public String serial;
}
