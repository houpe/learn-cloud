import com.lun.springcloud.dao.payMentDao;
import com.lun.springcloud.entity.PayMent;
import org.junit.jupiter.api.Test;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @Classname test
 * @Description TODO
 * @Date 2021/9/28 12:10 下午
 * @Created by houpeng
 */
@SpringBootTest(classes =test.class)
@MapperScan("com.lun.springcloud.dao")
@EnableAutoConfiguration
public class test {
    @Autowired
    private com.lun.springcloud.dao.payMentMapper payMentMapper;
    @Test
    public void  mapperTest(){
        PayMent pay=new PayMent();
        pay.setId(1);
        pay.setSerial("11");
        payMentMapper.insert(pay);
        System.out.println("1");

    }
}
