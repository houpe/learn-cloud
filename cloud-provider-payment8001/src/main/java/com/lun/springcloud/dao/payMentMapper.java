package com.lun.springcloud.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lun.springcloud.entity.PayMent;


/**
 * @Classname payMentMapper
 * @Description TODO
 * @Date 2021/9/28 11:48 上午
 * @Created by houpeng
 */
public interface payMentMapper extends BaseMapper<PayMent>
{
}
