package com.lun.springcloud;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @Classname payMentMain
 * @Description TODO
 * @Date 2021/9/28 11:20 上午
 * @Created by houpeng
 */
@SpringBootApplication
@MapperScan("com.lun.springcloud.dao")
@EnableEurekaClient
public class payMentMain {

    public static void main(String[] args) {
        SpringApplication.run(payMentMain.class);
    }

}
